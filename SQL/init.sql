-- Création de la base de données 'db' pour la SAE
CREATE DATABASE IF NOT EXISTS db
WITH ENCODING = 'UTF8';

-- Groupes d'utilisateurs
CREATE ROLE IF NOT EXISTS superadmin WITH SUPERUSER ON db;

CREATE ROLE IF NOT EXISTS editor WITH CREATEDB ON db;
GRANT SELECT, INSERT, UPDATE ON DATABASE db TO editor;

CREATE ROLE IF NOT EXISTS viewer ON db;
GRANT SELECT ON DATABASE db TO viewer;


-- Création des tables
DROP TABLE IF EXISTS Population, Event, EventClass, Department, Region CASCADE;

CREATE TABLE Region (
    reg_id              decimal(2)      PRIMARY KEY,
    name_reg            varchar(50)     NOT NULL
);

CREATE TABLE Department (
    dep_id 		        decimal(3)	    PRIMARY KEY,
	reg_id 		        decimal(2)      NOT NULL,
    name_dep 	        varchar(50)     NOT NULL,
    FOREIGN KEY reg_id REFERENCES Region(reg_id)
);

CREATE TABLE EventClass (
    class_id 			int			    PRIMARY KEY SERIAL,
    name 			    varchar(100)     NOT NULL
);

CREATE TABLE Event (
    id 					int			    PRIMARY KEY SERIAL,
    dep_id		        decimal(3)      NOT NULL,
    year 				int             NOT NULL,
    class_id 			int             NOT NULL,
    type			 	varchar(50)     NOT NULL,
	nbOccurrences		int             NOT NULL,
    thousandRates 		float           NOT NULL,
    FOREIGN KEY class_id REFERENCES EventClass(class_id),
    FOREIGN KEY dep_id REFERENCES Department(dep_id),
    CHECK (year >= 2016 AND year <= date_part('year', CURRENT_DATE)::int)
);

CREATE TABLE Population (
    pop_id 		        int 	        PRIMARY KEY SERIAL,
    dep_id 				decimal(3)      NOT NULL,
    year 				int             NOT NULL,
    millPop 			int             NOT NULL,
    millLog 			int             NOT NULL,
    pop 				float           NOT NULL,
    log 				float           NOT NULL,
    FOREIGN KEY dep_id REFERENCES Department(dep_id),
    CHECK (year >= 2016 AND year <= date_part('year', CURRENT_DATE)::int)
);