####################
# RAYNAUD Camille  #
# LAVERGNE Rémi    #
# CAILLOT Corentin #
# DELINAC Inès     #
# 1A-G8            #
#                  #
# 04/05/2024       #
####################

############ Imports ############
import getpass
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from sqlalchemy import create_engine, text, exc
import seaborn as sns
import pygal_maps_fr as maps #! pip install pygal_maps_fr & pip install lxml
import pygal


print("=========== Génération des graphiques (graph.py) ===========\n")
print("\t -- Informations de connexion --")

print("Le host est londres, voulez-vous changer le host? (y/n)")
host = "londres"

if input() == "y":
    host = str(input("Host: "))
else:
    host = "londres"

db_name = str(input("Nom de la base de données: "))
user = input("Nom d'utilisateur: ")
password = getpass.getpass("Mot de Passe: ")


co = None
# Connexion à la base de données (avec PostgreSQL)
engine = create_engine("postgresql://"+user+":"+password+"@"+host+"/"+db_name)

print("\t Connexion réussie !")

print("\t -- Génération des graphiques... --")

try:
    co = engine.connect()

    # ============== Graphique 1 ==============
    # Diagramme camembert pour afficher les types vols en 2019 et 2023
    result = co.execute( text('''
        SELECT e.year, ec.name, SUM(e.nbOccurrences) AS total_occurrences
        FROM Event e
        JOIN EventClass ec ON e.class_id = ec.class_id
        WHERE (e.year IN (2019, 2023)) 
            AND (ec.name LIKE 'Vol%')
        GROUP BY e.year, ec.name
        ORDER BY e.year, ec.name;
    '''))

    results = result.fetchall()

    data_2019 = {}
    data_2023 = {}
    for row in results:
        year = row[0]
        crime_type = row[1]
        total = row[2]
        
        if year == 2019:
            if crime_type not in data_2019:
                data_2019[crime_type] = total
        elif year == 2023:
            if crime_type not in data_2023:
                data_2023[crime_type] = total

    plt.figure(figsize=(16, 6))

    # 2019
    plt.subplot(1, 2, 1)
    plt.pie(
        data_2019.values(), 
        labels=None, 
        autopct='%1.1f%%', 
        colors=['#ff0000','#ff9999','#66b3ff','#99ff99','#ffcc99', '#c2c2f0'], 
        textprops={'fontsize': 10}, 
        startangle=140
    )
    plt.title(f"Répartition des types de vols en 2019\n(pour {sum(data_2019.values())} cas)")
    plt.axis('equal')
    plt.legend(data_2019.keys(), loc="upper left", bbox_to_anchor=(1, 1))

    # 2023
    plt.subplot(1, 2, 2)
    plt.pie(
        data_2023.values(), 
        labels=None, 
        autopct='%1.1f%%', 
        colors=['#ff0000','#ff9999','#66b3ff','#99ff99','#ffcc99', '#c2c2f0'], 
        textprops={'fontsize': 10}, 
        startangle=140
    )
    plt.title(f"Répartition des types de vols en 2023\n(pour {sum(data_2023.values())} cas)")
    plt.axis('equal')
    plt.legend(data_2023.keys(), loc="upper left", bbox_to_anchor=(1, 1))

    plt.tight_layout()
    plt.show()


    # ============== Graphique 2 ==============
    # Évolution dans le temps des crimes et délits pour les 6 régions les plus dangereuses
    result = co.execute( text('''
        SELECT r.name_reg, e.year, SUM(e.nbOccurrences) AS total_occurrences
        FROM Event e
        JOIN Department d ON e.dep_id = d.dep_id
        JOIN Region r ON d.reg_id = r.reg_id
        WHERE e.year >= 2016 AND e.year <= 2023
            AND r.name_reg IN (
                SELECT name_reg
                FROM (
                    SELECT r.name_reg, SUM(e.nbOccurrences) AS total_occurrences
                    FROM Event e
                    JOIN Department d ON e.dep_id = d.dep_id
                    JOIN Region r ON d.reg_id = r.reg_id
                    WHERE e.year >= 2016 AND e.year <= 2023
                    GROUP BY r.name_reg
                    ORDER BY total_occurrences DESC
                    LIMIT 6
                ) AS top_regions
            )
        GROUP BY r.name_reg, e.year
        ORDER BY r.name_reg, e.year;
    '''))

    region_data = {}
    for row in result:
        region = row[0]
        year = row[1]
        occurrences = row[2]
        if region not in region_data:
            region_data[region] = {'years': [], 'occurrences': []}
        region_data[region]['years'].append(year)
        region_data[region]['occurrences'].append(occurrences)

    # Création du graphique
    plt.figure(figsize=(10, 6))
    for region in region_data:
        plt.plot(region_data[region]['years'], region_data[region]['occurrences'], label=region)

    plt.xlabel('Année')
    plt.ylabel('Nombre d\'occurrences')
    plt.title('Évolution temporelle des crimes et délits pour les 6 régions les plus dangereuses\n(2016-2023)')
    plt.legend(title='Région', loc='center left')
    plt.grid()
    plt.show()


    # ================ Graphique 3 ================
    # Rsépartition des crimes par classe en 2023
    result = co.execute(text('''
        SELECT r.name_reg, SUM(e.nbOccurrences) AS total_occurrences
        FROM Event e
        JOIN Department d ON e.dep_id = d.dep_id
        JOIN Region r ON d.reg_id = r.reg_id
        WHERE e.year = 2023
        GROUP BY r.name_reg
        ORDER BY total_occurrences DESC;
    '''))

    region_names = []
    total_occurrences = []
    for row in result:
        region_names.append(row[0])
        total_occurrences.append(row[1])


    plt.figure(figsize=(10, 6))
    plt.bar(region_names, total_occurrences, color='skyblue')
    plt.xlabel('Région')
    plt.ylabel('Nombre total d\'occurrences de crimes et délits')
    plt.title('Comparaison des taux de criminalité entre les régions en 2023')
    plt.xticks(rotation=45, ha='right')
    plt.grid(axis='y') 
    plt.show()


    # ================ Graphique 5 =================
    # Population totale par région en 2023
    result = co.execute(text('''
        SELECT r.name_reg, SUM(p.pop) AS total_population
        FROM Population p
        JOIN Department d ON p.dep_id = d.dep_id
        JOIN Region r ON d.reg_id = r.reg_id
        WHERE p.yearPop = 21
        GROUP BY r.name_reg
        ORDER BY total_population DESC;
    '''))

    region_names = []
    total_population = []
    for row in result:
        region_names.append(row[0])
        total_population.append(row[1])

    plt.figure(figsize=(10, 6))
    plt.bar(region_names, total_population, color='lightgreen')
    plt.xlabel('Région')
    plt.ylabel('Population totale (en Millions)')
    plt.title('Comparaison des populations entre les régions en 2023\n(Année de référence : 2021)')
    plt.xticks(rotation=45, ha='right')
    plt.grid(axis='y')
    plt.show()


    # ================ Graphique 6 =================
    # Carte de la région Île-de-France avec les taux de délits et de crimes visualisés
    #! pip install pygal_maps_fr & pip install lxml
    result = co.execute(text('''
        SELECT d.dep_id, SUM(e.nbOccurrences) AS total_occurrences
        FROM Event e
        JOIN Department d ON e.dep_id = d.dep_id
        JOIN Region r ON d.reg_id = r.reg_id
        WHERE r.reg_id = '11' AND e.year = 2023
        GROUP BY d.dep_id;
    '''))

    crime_rates = {}
    for row in result:
        department = row[0]
        total_occurrences = row[1]
        crime_rates[department] = total_occurrences

    # Création de la carte de la région Île-de-France
    map_fr = pygal.maps.fr.Departments()
    map_fr.title = 'Taux de delits et de crimes par departement en Ile-de-France en 2023'
    map_fr.add('Taux', crime_rates)

    # Légende avec résumé des valeurs
    min_rate = min(crime_rates.values())
    max_rate = max(crime_rates.values())
    map_fr.add('Min : '+str(min_rate), {'Min': min_rate})
    map_fr.add('Max : '+str(max_rate), {'Max': max_rate})

    # Affichage de la carte
    map_fr.render_in_browser() #! pip install pygal_maps_fr & pip install lxml


    # ================ Graphique 7 =================
    # Comparaison entre le nombre de cambriolage et le nombre de logement dans chaque département (Heatmap)
    result = co.execute(text('''
        SELECT e.nbOccurrences, p.pop, p.log
        FROM Event e
        JOIN Population p ON e.dep_id = p.dep_id AND e.year = p.year;
    '''))

    # matrice de corrélation
    data = np.array([row for row in result])

    # Calcul de la corrélation
    correlation_matrix = np.corrcoef(data, rowvar=False)

    # Création de la heatmap avec Matplotlib et Seaborn
    plt.figure(figsize=(8, 6))
    sns.heatmap(correlation_matrix, annot=True, cmap='coolwarm', fmt=".2f", xticklabels=['Crimes', 'Population', 'Logements'], yticklabels=['Crimes', 'Population', 'Logements'])
    plt.title('Corrélation entre le nombre de crimes, la population et le nombre de logements')
    plt.tight_layout()
    plt.show()


    # ================ Graphique 8 =================
    # Répartition des crimes par type en 2016 et 2023
    result_crime_types = co.execute(text('''
        SELECT year, ec.name, SUM(nbOccurrences) AS total_occurrences
        FROM Event e
        JOIN EventClass ec ON e.class_id = ec.class_id              
        WHERE year = 2016 OR year = 2023
        GROUP BY year, ec.name
        ORDER BY year, total_occurrences DESC;
    '''))

    crime_types_data = {}
    for row in result_crime_types:
        year = row[0]
        crime_type = row[1]
        occurrences = row[2]
        if year not in crime_types_data:
            crime_types_data[year] = {'types': [], 'occurrences': []}
        crime_types_data[year]['types'].append(crime_type)
        crime_types_data[year]['occurrences'].append(occurrences)


    for year, data in crime_types_data.items():
        plt.figure(figsize=(10, 6))
        plt.bar(data['types'], data['occurrences'])
        plt.title(f'Répartition des crimes par type en {year}')
        plt.xlabel('Type de crime')
        plt.ylabel('Nombre d\'occurrences')
        plt.xticks(rotation=45, ha='right')
        plt.tight_layout()
        plt.show()


    # ================ Graphique 9 =================
    # évolution du nombre total de crimes au fil des années
    result_total_crimes = co.execute(text('''
        SELECT year, SUM(nbOccurrences) AS total_crimes
        FROM Event
        WHERE year >= 2016 AND year <= 2023
        GROUP BY year
        ORDER BY year
    '''))
    years, total_crimes = zip(*result_total_crimes)

    plt.figure(figsize=(10, 6))
    plt.plot(years, total_crimes, marker='o', linestyle='-')
    plt.title('Évolution du nombre total de crimes (2016-2023)')
    plt.xlabel('Année')
    plt.ylabel('Nombre total de crimes')
    plt.grid(True)
    plt.tight_layout()
    plt.show()

except (exc.SQLAlchemyError) as error:
    print("\t Erreur lors de la génération des graphiques:", error)
finally:
    if co is not None:
        co.close()
