####################
# RAYNAUD Camille  #
# LAVERGNE Rémi    #
# CAILLOT Corentin #
# DELINAC Inès     #
# 1A-G8            #
#                  #
# 04/05/2024       #
####################

############ Imports ############
import getpass
import pandas as pd
from sqlalchemy import text, create_engine, exc

DEPARTMENTS = {
    '01': 'Ain', 
    '02': 'Aisne', 
    '03': 'Allier', 
    '04': 'Alpes-de-Haute-Provence', 
    '05': 'Hautes-Alpes',
    '06': 'Alpes-Maritimes', 
    '07': 'Ardèche', 
    '08': 'Ardennes', 
    '09': 'Ariège', 
    '10': 'Aube', 
    '11': 'Aude',
    '12': 'Aveyron', 
    '13': 'Bouches-du-Rhône', 
    '14': 'Calvados', 
    '15': 'Cantal', 
    '16': 'Charente',
    '17': 'Charente-Maritime', 
    '18': 'Cher', 
    '19': 'Corrèze', 
    '2A': 'Corse-du-Sud', 
    '2B': 'Haute-Corse',
    '21': 'Côte-d\'Or', 
    '22': 'Côtes-d\'Armor', 
    '23': 'Creuse', 
    '24': 'Dordogne', 
    '25': 'Doubs', 
    '26': 'Drôme',
    '27': 'Eure', 
    '28': 'Eure-et-Loir', 
    '29': 'Finistère', 
    '30': 'Gard', 
    '31': 'Haute-Garonne', 
    '32': 'Gers',
    '33': 'Gironde', 
    '34': 'Hérault', 
    '35': 'Ille-et-Vilaine', 
    '36': 'Indre', 
    '37': 'Indre-et-Loire',
    '38': 'Isère', 
    '39': 'Jura', 
    '40': 'Landes', 
    '41': 'Loir-et-Cher', 
    '42': 'Loire', 
    '43': 'Haute-Loire',
    '44': 'Loire-Atlantique', 
    '45': 'Loiret', 
    '46': 'Lot', 
    '47': 'Lot-et-Garonne', 
    '48': 'Lozère',
    '49': 'Maine-et-Loire', 
    '50': 'Manche', 
    '51': 'Marne', 
    '52': 'Haute-Marne', 
    '53': 'Mayenne',
    '54': 'Meurthe-et-Moselle', 
    '55': 'Meuse', 
    '56': 'Morbihan', 
    '57': 'Moselle', 
    '58': 'Nièvre', 
    '59': 'Nord',
    '60': 'Oise', 
    '61': 'Orne', 
    '62': 'Pas-de-Calais', 
    '63': 'Puy-de-Dôme', 
    '64': 'Pyrénées-Atlantiques',
    '65': 'Hautes-Pyrénées', 
    '66': 'Pyrénées-Orientales', 
    '67': 'Bas-Rhin', 
    '68': 'Haut-Rhin', 
    '69': 'Rhône',
    '70': 'Haute-Saône', 
    '71': 'Saône-et-Loire', 
    '72': 'Sarthe', 
    '73': 'Savoie', 
    '74': 'Haute-Savoie',
    '75': 'Paris', 
    '76': 'Seine-Maritime', 
    '77': 'Seine-et-Marne', 
    '78': 'Yvelines', 
    '79': 'Deux-Sèvres',
    '80': 'Somme', 
    '81': 'Tarn', 
    '82': 'Tarn-et-Garonne', 
    '83': 'Var', 
    '84': 'Vaucluse', 
    '85': 'Vendée',
    '86': 'Vienne', 
    '87': 'Haute-Vienne', 
    '88': 'Vosges', 
    '89': 'Yonne', 
    '90': 'Territoire de Belfort',
    '91': 'Essonne', 
    '92': 'Hauts-de-Seine', 
    '93': 'Seine-Saint-Denis', 
    '94': 'Val-de-Marne', 
    '95': 'Val-d\'Oise',
    '971': 'Guadeloupe', 
    '972': 'Martinique', 
    '973': 'Guyane', 
    '974': 'La Réunion', 
    '976': 'Mayotte'
}

REGIONS = {
    '01': 'Guadeloupe',
    '02': 'Martinique',
    '03': 'Guyane',
    '04': 'La Réunion',
    '06': 'Mayotte',
    '11': 'Île-de-France',
    '24': 'Centre-Val de Loire',
    '27': 'Bourgogne-Franche-Comté',
    '28': 'Normandie',
    '32': 'Hauts-de-France',
    '44': 'Grand Est',
    '52': 'Pays de la Loire',
    '53': 'Bretagne',
    '75': 'Nouvelle-Aquitaine',
    '76': 'Occitanie',
    '84': 'Auvergne-Rhône-Alpes',
    '93': 'Provence-Alpes-Côte d\'Azur',
    '94': 'Corse'
}

DEPARTMENTS_TO_REGIONS = {
    '01': ['971'],
    '02': ['972'],
    '03': ['973'],
    '04': ['974'],
    '06': ['976'],
    '11': ['75','77','78','91','92','93','94','95'],
    '24': ['18', '28', '36', '37', '41', '45'],
    '27': ['21', '25', '39', '58', '70', '71', '89', '90'],
    '28': ['14', '27', '50', '61', '76'],
    '32': ['02', '59', '60', '62', '80'],
    '44': ['08', '10', '51', '52', '54', '55', '57', '67', '68', '88'],
    '52': ['44', '49', '53', '72', '85'],
    '53': ['35', '22', '56', '29'],
    '75': ['16', '17', '19', '23', '24', '33', '40', '47', '64', '79', '86', '87'],
    '76': ['09', '11', '12', '30', '31', '32', '34', '46', '48', '65', '66', '81', '82'],
    '84': ['01', '03', '07', '15', '26', '38', '42', '43', '63', '69', '73', '74'],
    '93': ['04', '05', '06', '13', '83', '84'],
    '94': ['2A', '2B']
}



print("=========== Création de la Base de Données (init.py) ===========\n")
print("\t -- Informations de connexion --")

print("Le host est londres, voulez-vous changer le host? (y/n)")
host = "londres"

if input() == "y":
    host = str(input("Host: "))
else:
    host = "londres"

db_name = str(input("Nom de la base de données: "))
user = input("Nom d'utilisateur: ")
password = getpass.getpass("Mot de Passe: ")


co = None
# Connexion à la base de données (avec PostgreSQL)
engine = create_engine("postgresql://"+user+":"+password+"@"+host+"/"+db_name)

print("\t Connexion réussie !")

print("\t -- Création des tables... --")

try:
    co = engine.connect()
    # Suppression des tables si elles existent déjà
    #! ATTENTION, supprime toute les données à l'intérieur de celles-ci !
    co.execute(text('''
        DROP TABLE IF EXISTS Population CASCADE;
        DROP TABLE IF EXISTS Event CASCADE;
        DROP TABLE IF EXISTS EventClass CASCADE;
        DROP TABLE IF EXISTS Department CASCADE;
        DROP TABLE IF EXISTS Region CASCADE;
    '''))

    # Création de la table Region
    co.execute(text('''
        CREATE TABLE Region (
            reg_id      varchar(2)      PRIMARY KEY,
            name_reg    varchar(50)     NOT NULL    UNIQUE
        );
    '''))

    # Création de la table Department
    co.execute(text('''
        CREATE TABLE Department (
            dep_id      varchar(3)	    PRIMARY KEY,
            reg_id      varchar(2)      NOT NULL,
            name_dep    varchar(50)     NOT NULL    UNIQUE,
            FOREIGN KEY (reg_id) REFERENCES Region(reg_id)
        );
    '''))

    # Création de la table EventClass
    co.execute(text('''
        CREATE TABLE EventClass (
            class_id    SERIAL			    PRIMARY KEY ,
            name        varchar(100)    NOT NULL
        );
    '''))
    
    # Création de la table Event
    co.execute(text('''
        CREATE TABLE Event (
            id 					SERIAL			PRIMARY KEY,
            dep_id		        varchar(3)      NOT NULL,
            year 				int             NOT NULL,
            class_id 			int             NOT NULL,
            type			 	varchar(50)     NOT NULL,
            nbOccurrences		int             NOT NULL,
            thousandRates 		float           NOT NULL,
            FOREIGN KEY (class_id) REFERENCES EventClass(class_id),
            FOREIGN KEY (dep_id) REFERENCES Department(dep_id),
            CHECK (year >= 2016 AND year <= date_part('year', CURRENT_DATE)::int)
        );
    '''))

    # Création de la table Population
    co.execute(text('''
        CREATE TABLE Population (
            dep_id 				varchar(3)      NOT NULL,
            year                int             NOT NULL,
            yearPop 			int             NOT NULL,
            yearLog 			int             NOT NULL,
            pop 				int             NOT NULL,
            log 				int             NOT NULL,
            PRIMARY KEY (dep_id, year),
            FOREIGN KEY (dep_id) REFERENCES Department(dep_id),
            CHECK (year >= 2016 AND year <= date_part('year', CURRENT_DATE)::int)
        );
    '''))

    co.commit()

    # Valider les modifications dans la base de données
    
    print("\t Base de Données créée avec succès!")
    print("\t > Tables: Population, Event, EventClass, Department, Region")
    print("\t ========================================")
    print("\t -- Insertion des données... --")

    # ==================== REMPLISSAGE DES TABLES ==================== #

    df = pd.read_csv('dataset.csv', delimiter=';')

    # Nettoyage
    df_cleaned = df.drop_duplicates() # Supprimer les doublons
    df_cleaned = df_cleaned.replace("NA", '')
    df_cleaned = df_cleaned.replace(',', '.', regex=True)
    df_cleaned = df_cleaned.dropna() # Supprimer les lignes avec des valeurs nulles
   
    # Renommage des colonnes
    df_cleaned.rename(columns={
        "classe": "class_name",
        "annee": "year",
        "Code.département": "dep_id",
        "Code.région": "reg_id",
        "unité.de.compte": "type",
        "millPOP": "yearPop",
        "millLOG": "yearLog",
        "faits": "nbOccurrences",
        "POP": "pop",
        "LOG": "log",
        "tauxpourmille": "thousandRates"
    }, inplace=True)

    # Change les types de données (pour la base de données) #
    df_cleaned["dep_id"] = df_cleaned["dep_id"].astype(str)

    df_cleaned["reg_id"] = df_cleaned["reg_id"].astype(int) #? On le passe en int d'abord pour éviter les virgules
    df_cleaned["reg_id"] = df_cleaned["reg_id"].astype(str)

    df_cleaned["year"] = df_cleaned["year"].astype(int)
    df_cleaned["nbOccurrences"] = df_cleaned["nbOccurrences"].astype(int)
    df_cleaned["pop"] = df_cleaned["pop"].astype(int)

    df_cleaned["log"] = df_cleaned["log"].astype(float) #? On force le passage de string à float pour d'abord éviter les erreurs, puis en int pour retirer les décimales
    df_cleaned["log"] = df_cleaned["log"].astype(int)
    
    df_cleaned["thousandRates"] = df_cleaned["thousandRates"].astype(float)

    # zéro devant si la longueur de la chaîne est inférieure à 2 (car les numéros ont toujours au moins deux chiffres avec un zéro devant)
    df_cleaned["dep_id"] = df_cleaned["dep_id"].str.zfill(2)
    df_cleaned["reg_id"] = df_cleaned["reg_id"].str.zfill(2)


    df_regions = df_cleaned["reg_id"].drop_duplicates().to_frame()
    df_departments = df_cleaned["dep_id"].drop_duplicates().to_frame()

    # Ajout des noms de régions et de départements (avec map qui compare la clé)
    df_regions['region_name'] = df_regions["reg_id"].map(REGIONS)
    df_departments['department_name'] = df_departments["dep_id"].map(DEPARTMENTS) 

    # Dataframe pour la relation entre départements et régions
    departments_region = []
    for region_code, department_list in DEPARTMENTS_TO_REGIONS.items():
        for department_code in department_list:
            departments_region.append((department_code, region_code))

    df_departments_region = pd.DataFrame(departments_region, columns=['dep_id', 'region_id'])

    # Dataframe pour les classes d'événements (pour éviter les doublons)
    df_classes = df_cleaned["class_name"].drop_duplicates().to_frame()

    # Dataframe pour les données de population (triées par département et année)
    df_population = df_cleaned[["dep_id", "year", "yearPop", "yearLog", "pop", "log"]].drop_duplicates().sort_values(by=['dep_id', 'year'])


    for row in df_regions.itertuples():
        co.execute( text('''
                        INSERT INTO Region (reg_id, name_reg)
                        VALUES (:reg_id, :name_reg);
                        '''), {'reg_id': row.reg_id, 'name_reg': row.region_name})
    

    for row in df_departments.itertuples():
        match_region = df_departments_region[df_departments_region['dep_id'] == row.dep_id]['region_id'].values[0]
        co.execute( text('''
                        INSERT INTO Department (dep_id, reg_id, name_dep)
                        VALUES (:dep_id, :reg_id, :name_dep);
                        '''), {'dep_id': row.dep_id, 'reg_id': match_region, 'name_dep': row.department_name})

    for row in df_classes.itertuples():
        co.execute( text('''
                        INSERT INTO EventClass (name)
                        VALUES (:class_name);
                        '''), {'class_name': row.class_name})
        
    for row in df_population.itertuples():
        # On transforme l'année xx en 20xx pour respecter la contrainte (event_year_check)
        year = row.year
        if year < 2000:
            year = year + 2000

        co.execute( text('''
                        INSERT INTO Population (dep_id, year, yearPop, yearLog, pop, log)
                        VALUES (:dep_id, :year, :yearPop, :yearLog, :pop, :log);
                        '''), {'dep_id': row.dep_id, 'year': year, 'yearPop': row.yearPop, 'yearLog': row.yearLog, 'pop': row.pop, 'log': row.log})

    for row in df_cleaned.itertuples():
        # On transforme l'année xx en 20xx pour respecter la contrainte (event_year_check)
        year = row.year
        if year < 2000:
            year = year + 2000

        co.execute( text('''
                        INSERT INTO Event (dep_id, year, class_id, type, nbOccurrences, thousandRates)
                        VALUES (:dep_id, :year, (SELECT class_id FROM EventClass WHERE name = :class_name), :type, :nbOccurrences, :thousandRates);
                        '''), {'dep_id': row.dep_id, 'year': year, 'class_name': row.class_name, 'type': row.type, 'nbOccurrences': row.nbOccurrences, 'thousandRates': row.thousandRates})
    

    co.commit()

    print("\t Données insérées avec succès!")
    
except (exc.SQLAlchemyError) as error:
    print("\t Erreur lors de la création de la Base de Données:", error)
    
finally:
    # Fermer la connexion et le curseur
    if co is not None:
        co.close()